import {mapState, mapActions, mapMutations} from 'vuex';
import config from '../config.js';
let jweixin = require('weixin-js-sdk');
import rsa from './rsa';
export default {
    computed: {
        ...mapState([
            'statusClass',
            'use_url',
            'url',
            'lifetime',
            'theme',
            'wechat_appid'
        ])
    },
    methods: {
        ...mapActions({
            refreshToken: 'refreshToken',
            pushMessage: 'pushMessage',
            refreshClientId: 'refreshClientId',
            logout: 'user/logout', //退出登录
        }),
        ...mapMutations({
            setLanguage: 'setLanguage',
        }),
        upCookie() {
            let authorization = getCookie('Authorization');
            if (authorization) {
                setCookie('Authorization', authorization, this.lifetime / 1440);
            }
        },
        //初始化微信公众号
        initOfficialAccount(){
            let ua = navigator.userAgent.toLowerCase();
            if(
                ua.match(/MicroMessenger/i)=="micromessenger" //是微信打开
            ) {
                axios.get(this.use_url+'/open/wx-config').then( (response)=> {
                    let official_account = response.data;
                    if(official_account //配置
                        && official_account.wxconfig
                        && official_account.wxconfig.appId){
                        jweixin.config(official_account.wxconfig);
                        jweixin.ready(()=>{
                            let obj = official_account.show;
                            jweixin.updateAppMessageShareData(obj);
                            jweixin.updateTimelineShareData(obj);
                            jweixin.onMenuShareTimeline(obj);
                            jweixin.onMenuShareAppMessage(obj);
                            jweixin.onMenuShareQQ(obj);
                            jweixin.onMenuShareWeibo(obj);
                            jweixin.onMenuShareQZone(obj);
                        });
                    }
                }).catch((error) => {
                    dd(error);
                });

            }
        }
    },
    created() {
        //默认语言包
        this.setLanguage(config.language);
        $('body').addClass(this.theme);
        //添加弹窗拦截器
        window.axios.interceptors.response.use((response) => {
                let headers = response.headers || {};
                if(headers['response-encryption']==1){ //响应加密先对数据进行解密
                    let raw_data = response.data;
                    let app_key = window.AppConfig.app_key || rsa.app_key;
                    let decode_data = window.AppConfig.decryptData(raw_data,rsa.client_rsa,app_key);
                    if(decode_data){
                        response.data = decode_data;
                    }
                }
                //跳转
                if (typeof response.data != 'undefined' &&
                    typeof response.data.force_logout != 'undefined' &&
                    response.data.force_logout
                ) {
                    this.logout({
                        force_logout:true,
                        data:{
                            key:'modal',
                            modal:{
                                title:this.$t('Prompt'),
                                content: this.$t('Your account has been logged in elsewhere'),
                                hideCancel:true
                            }
                        }
                    });
                } else if (typeof response.data != 'undefined' &&
                    typeof response.data.redirect != 'undefined' &&
                    response.data.redirect
                ) {
                    if (response.data.redirect.indexOf('http://') == 0 ||
                        response.data.redirect.indexOf('https://') == 0
                    ) { //跳转外部页面
                        window.location.href = response.data.redirect;
                    } else {
                        if (response.status == 200) {
                            this.$router.push({path: response.data.redirect}).catch(error => {
                                dd(error.message);
                            });
                        } else {
                            this.$router.replace({path: response.data.redirect}).catch(error => {
                                dd(error.message);
                            });
                        }
                    }
                }
                //消息提醒
                if (typeof response.data != 'undefined' &&
                    typeof response.data.alert != 'undefined' &&
                    response.data.alert
                ) {
                    this.pushMessage(response.data.alert);
                }
                //登录信息更新
                if (!response.data._token) {
                    this.upCookie();
                }
                return response;
            },
            (error) => {
                let response = error.response;
                let headers = response.headers || {};
                if(headers['response-encryption']==1){ //响应加密先对数据进行解密
                    let raw_data = response.data;
                    let app_key = window.AppConfig.app_key || rsa.app_key;
                    let decode_data = window.AppConfig.decryptData(raw_data,rsa.client_rsa,app_key);
                    if(decode_data){
                        response.data = decode_data;
                    }
                }
                //跳转
                if (typeof error.response != 'undefined' &&
                    typeof error.response.data != 'undefined' &&
                    typeof error.response.data.force_logout != 'undefined' &&
                    error.response.data.force_logout
                ) {
                    this.logout({
                        force_logout:true,
                        data:{
                            key:'modal',
                            modal:{
                                title:this.$t('Prompt'),
                                content: this.$t('Your account has been logged in elsewhere'),
                                hideCancel:true
                            }
                        }
                    });
                } else if (typeof error.response != 'undefined' &&
                    typeof error.response.data != 'undefined' &&
                    typeof error.response.data.redirect != 'undefined' &&
                    error.response.data.redirect
                ) {
                    this.$router.replace({path: error.response.data.redirect}).catch(error => {
                        dd(error.message);
                    });
                } else if (error.response.status == 503) {
                    this.$router.push({path: '/503'}).catch(error => {
                    });
                } else if (!error.response || (error.response && error.response.status != 422 && error.response.status != 200)) {
                    let message = {
                        'showClose': true, //显示关闭按钮
                        'title': this.$t('An error occurred while requesting the server, please contact the developer in time'),//'请求服务器时发生错误,请及时联系开发人员!', //消息内容
                        'message': '', //消息内容
                        'type': 'danger', //消息类型
                        'position': 'top',
                        'iconClass': 'fa-close', //图标
                        'customClass': '', //自定义样式
                        'duration': 3000, //显示时间毫秒
                        'show': true //是否自动弹出
                    };

                    //token过期
                    if (error.response.status == 419 && error.response.data.message.indexOf('token') != -1) {
                        this.refreshToken();
                        message.title = this.$t('Token has expired, please try again');
                    }

                    if (error.response.data.alert) {
                        this.pushMessage(error.response.data.alert);
                    } else {
                        this.pushMessage(message);
                    }
                }
                return Promise.reject(error);
            });

        /**
         * ajax提交请求
         * 参数格式化
         */
        window.axios.interceptors.request.use(function (config) {
            let token = getCookie('Authorization');
            if (token) {
                config.headers.Authorization = decodeURIComponent(token);
            }
            let open_id = getCookie('Open-Id');
            if (open_id) {
                config.headers['Open-Id'] = decodeURIComponent(open_id);
            }
            let language = localStorage.getItem('language');
            if (language) {
                config.headers.Language = decodeURIComponent(language);
            }
            config.paramsSerializer = function (params) {
                params = JSON.parse(JSON.stringify(params));
                params['json'] = 1;
                return jQuery.param(params);
            };
            config.headers.RefererPage = window.location.href;
            let api_encryption = 0;
            if(window.AppConfig.api_encryption && //允许加密提交
                rsa.server_rsa_pub && //有服务端公钥
                config.method!='get' && //非get请求
                !window.AppConfig.debug //调试模式不加密
            ){ //判断是否可使用加密传输
                config.headers['Api-Encryption'] = api_encryption = 1;
                let data = config.data;
                if (typeof data === 'object') {
                    let app_key = window.AppConfig.app_key || rsa.app_key;
                    let encrypt_data = window.AppConfig.encryptData(JSON.stringify(config.data),rsa.server_rsa_pub,app_key);
                    config.data = encrypt_data;
                }
            }
            if(window.AppConfig.api_encryption && //允许加密响应
                rsa.client_rsa && //
                !window.AppConfig.debug
            ){//是否支持响应加密
                config.headers['Support-Response-Encryption'] = 1;
            }
            return config;
        }, function (error) {
            return Promise.reject(error);
        });
        window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        window.axios.defaults.withCredentials = true;
        //获取连接ID唯一标识
        this.refreshClientId();
        //CSRF _token更新
        setTimeout(() => {
            this.refreshToken();
        }, 1500);
        setInterval(() => {
            this.refreshToken();
        }, 7200000);
    },
    watch:{
        $route(to,from){
            if(this.wechat_appid){
                this.initOfficialAccount();
            }
        }
    }


};
