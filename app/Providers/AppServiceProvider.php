<?php

namespace App\Providers;

use App\Facades\LifeData;
use App\Models\Traits\DbMysqlImplModel;
use App\Models\Traits\NestedSetsService;
use App\Services\ClientAuth;
use App\Services\FormatterService;
use App\Services\LifeDataRepository;
use App\Services\OptionRepository;
use App\Services\SMSNewService;
use App\Swoole\Coroutine\Redis\PhpRedisConnector;
use App\Validators\CustomValidator;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Database\Connectors\ConnectionFactory;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use SwooleTW\Http\Server\Manager;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;
use App\Handler\FeishuWebhookHandler;
use App\Handler\WorkWeixinWebhookHandler;
use Monolog\Logger as Monolog;
use Spatie\Backup\Tasks\Backup\DbDumperFactory;
use Spatie\DbDumper\Databases\MySql;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //移动端唯一标识生成
        $this->app->singleton('app_client', ClientAuth::class);
        //返回数据存放
        $this->app->singleton('life_data', LifeDataRepository::class);
        //系统配置
        $this->app->singleton('option', OptionRepository::class);
        //数据结构格式化
        $this->app->singleton('formatter', FormatterService::class);
        //极验验证码
        $this->app->singleton('geetest', function () {
            return $this->app->make('ZBrettonYe\Geetest\GeetestLib');
        });

        //短信发送
        $this->app->singleton('sms' , function($app){
            return new SMSNewService();
        });
        $this->app->singleton(Manager::class, function ($app) {
            return new \App\Swoole\Server\Manager($app, 'laravel');
        });

        $this->app->alias(Manager::class, 'swoole.manager');
        $this->app->singleton(ConnectionFactory::class, function ($app) {
            return new \App\Swoole\Coroutine\Database\ConnectionFactory($app);
        });
        $this->app->alias(ConnectionFactory::class,'db.factory');

        Log::extend('feishu',function ($app,$config){
            return new Monolog($this->parseChannel($config), [
                $this->prepareHandler(new FeishuWebhookHandler($config)),
            ]);
        });
        Log::extend('work_weixin',function ($app,$config){
            return new Monolog($this->parseChannel($config), [
                $this->prepareHandler(new WorkWeixinWebhookHandler($config)),
            ]);
        });
        if(class_exists('Spatie\\Backup\\Tasks\\Backup\\DbDumperFactory')){
            DbDumperFactory::extend('swoole-mysql',function (){
                return new MySql();
            });
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(ResponseFactory $factory)
    {
        Redis::extend('swoole-phpredis', function () {
            return new PhpRedisConnector();
        });
        Schema::defaultStringLength(191);
        //响应返回
        $factory->macro('returns', function ($value=[],$status=Response::HTTP_OK,array $headers = []) use ($factory) {
            $value = collect(LifeData::all())->only(['options','list'])->merge($value)->toArray();
            collect($value)->each(function ($value,$key){
                LifeData::set($key,$value);
            });
            if(Request::input('callback')){ //jsonp
                return $factory->jsonp(Request::input('callback'),$value,$status);
                $headers['Content-Type'] = 'application/javascript';
            }elseif(Request::input('define')=='AMD'){ //AMD
                $value = 'define([],function(){ return '.collect($value)->toJson().';});';
                $headers['Content-Type'] = 'application/javascript';
            }elseif(Request::input('define')=='CMD'){ //CMD
                $value = 'define([],function(){ return '.collect($value)->toJson().';});';
                $headers['Content-Type'] = 'application/javascript';
            }elseif(Request::has('dd')){ //数据打印页面
                dd($value->toArray());
            }elseif(Request::has('script')){ //页面
                $value = 'var '.Request::input('script').' = '.collect($value)->toJson().';';
                $headers['Content-Type'] = 'application/javascript';
            }else{
                return $factory->json($value,$status,$headers);
            }
            return $factory->make($value,$status,$headers);
        });

        //注册自定义验证
        $this->app['validator']->resolver(function($translator, $data, $rules, $messages=[],$customAttributes=[])
        {
            return new CustomValidator($translator, $data, $rules, $messages,$customAttributes);
        });
        $this->app->singleton('DbMysqlModel', function($app)
        {
            return new DbMysqlImplModel();
        });
        $this->app->singleton('NestedSetsService', function($app)
        {
            return new NestedSetsService($app['DbMysqlModel']);
        });
    }
}
